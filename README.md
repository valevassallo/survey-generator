# Survey Generator

## Description

You can create customized surveys, and see all the past surveys you've created.

## Technologies used

- Django
- Django REST Framework
- React

## Installation/Usage

- Clone this repository with SSH: `git clone git@gitlab.com:valevassallo/survey-generator.git`
- `cd` into it: `cd survey-generator`
- Create a virtualenv `env` and activate it by running `source env/bin/activate`
- `cd` into `server`
- Install dependencies by running `pip install -r requirements.txt` (or `pip3 install -r requiremente.txt` if usinng Python3)
- Create new Postgres database
- Create new .env file using the .env.example as guidance
- Start server: `python manage.py runserver` (or `python3 manage.py runserver` if using Python3)
- Open new terminal and `cd` into `client`
- Install dependencies by running: `yarn install`
- Start client: `yarn start`
