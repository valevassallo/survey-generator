/** @jsx jsx */
import React from "react";
import { jsx } from "@emotion/core";
import axios from "axios";

import Survey from "../components/Survey";

function Surveys() {
  const [surveys, setSurveys] = React.useState([]);
  React.useEffect(() => {
    axios.get("http://localhost:8000/api/surveys/").then(res => {
      const surveys = res.data;
      setSurveys(surveys);
    });
  }, []);

  const container = {
    padding: 24,
    display: "flex",
    wrap: "wrap",
    justifyContent: "space-around"
  };

  return (
    <div css={container}>
      {surveys.map((survey, index) => {
        return <Survey key={index} survey={survey} />;
      })}
    </div>
  );
}

export default Surveys;
