/** @jsx jsx */
import React from "react";
import { jsx } from "@emotion/core";
import axios from "axios";

function SurveyDetails({ surveyId }) {
  const [currentSurvey, setCurrentSurvey] = React.useState({});
  React.useEffect(() => {
    axios.get(`http://localhost:8000/api/surveys/${surveyId}`).then(res => {
      const currentSurvey = res.data;
      setCurrentSurvey(currentSurvey);
    });
  }, []);

  const container = {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "left",
    padding: 16,
    borderRadius: 10,
    background: "white",
    mixBlendMode: "hard-light",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    width: 800,
    margin: "0 auto",
    marginTop: 24
  };

  return (
    <div css={container}>
      <h1 css={{ margin: 0, textAlign: "center" }}>{currentSurvey.title}</h1>

      {currentSurvey &&
        currentSurvey.questions &&
        currentSurvey.questions.map((question, index) => {
          return (
            <div key={index}>
              <h2 css={{ margin: "4px 0px" }}>{question.actual_question}</h2>
              <p css={{ margin: "8px 0px" }}>
                Question type: {question.question_type}
              </p>
              {question.question_type == "SHORT" ? (
                <input type="text" />
              ) : question.question_type == "LONG" ? (
                <textarea />
              ) : (
                <>
                  <label htmlFor="yes">Yes</label>
                  <input
                    css={{ marginRight: 8 }}
                    type="radio"
                    name="yes-no"
                    id="yes"
                    value="YES"
                  />
                  <label htmlFor="no">No</label>
                  <input type="radio" name="yes-no" id="no" value="NO" />
                </>
              )}
            </div>
          );
        })}
    </div>
  );
}

export default SurveyDetails;
