/** @jsx jsx */
import React from "react";
import { jsx } from "@emotion/core";
import axios from "axios";
import { navigate } from "@reach/router";

function NewSurvey() {
  const [number, setNumber] = React.useState("");

  function handleChange(event) {
    setNumber(event.target.value);
  }

  let dataToSend = {};
  let questions = [];

  function handleSubmit(event) {
    event.preventDefault();
    Array.from({ length: number }, (_, i) => {
      let question = {
        actual_question: event.target.elements[`q-${i}`].value,
        question_type: event.target.elements[`qt-${i}`].value
      };
      questions.push(question);
    });
    let dataToSend = {
      title: event.target.elements.title.value,
      questions_number: number,
      questions: questions
    };
    axios.post("http://localhost:8000/api/surveys/", dataToSend);
    navigate("/");
  }

  const formContainer = {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: 16,
    borderRadius: 10,
    background: "white",
    mixBlendMode: "hard-light",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    width: 800,
    margin: "0 auto",
    marginTop: 24
  };

  const inputCss = {
    width: 400,
    borderWidth: 3,
    borderColor: "black",
    borderRadius: 25,
    backgroundColor: "transparent",
    fontSize: 16,
    height: 20,
    padding: 8,
    color: "black",
    marginTop: 8,
    textAlign: "center"
  };

  const selectCss = {
    width: 400,
    borderWidth: 3,
    borderColor: "black",
    backgroundColor: "transparent",
    fontSize: 16,
    height: 32,
    padding: 8,
    color: "black",
    marginTop: 8
  };

  const buttonCss = {
    background: "#07101E",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 50,
    color: "white",
    padding: "8px 16px",
    fontSize: 20,
    fontWeight: 500,
    marginTop: 24
  };

  return (
    <form css={formContainer} onSubmit={handleSubmit}>
      <label css={{ marginTop: 16, fontSize: 18 }} htmlFor="title">
        Survey title
      </label>
      <input
        css={inputCss}
        id="title"
        name="title"
        type="text"
        autoComplete="off"
      />
      <label css={{ marginTop: 16, fontSize: 18 }} htmlFor="number">
        Questions number
      </label>
      <input
        css={inputCss}
        onChange={handleChange}
        value={number}
        id="number"
        type="number"
      />
      {number &&
        Array.from({ length: number }, (_, i) => {
          return (
            <div
              css={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
              key={i}
            >
              <label css={{ marginTop: 16, fontSize: 18 }} htmlFor={`q-${i}`}>
                Question {i + 1}
              </label>
              <input
                css={inputCss}
                type="text"
                id={`q-${i}`}
                name={`q-${i}`}
                autoComplete="off"
              />
              <label css={{ marginTop: 16, fontSize: 18 }} htmlFor={`qt-${i}`}>
                Question type
              </label>
              <select name={`qt-${i}`} css={selectCss}>
                <option disabled selected>
                  Select Question Type
                </option>
                <option id={`qt-${i}`} value="Y/N">
                  Yes/No Question
                </option>
                <option id={`qt-${i}`} value="SHORT">
                  Short Answer
                </option>
                <option id={`qt-${i}`} value="LONG">
                  Long Answer
                </option>
              </select>
            </div>
          );
        })}
      <button css={buttonCss} type="submit">
        Submit
      </button>
    </form>
  );
}

export default NewSurvey;
