import React from "react";
import { Router } from "@reach/router";
import { Global } from "@emotion/core";

import Header from "./components/Header";
import Surveys from "./views/Surveys";
import SurveyDetails from "./views/SurveyDetails";
import NewSurvey from "./views/NewSurvey";

function App() {
  return (
    <>
      <Global
        styles={{
          body: {
            fontFamily: "'Alegreya', serif",
            background: "#EFF8FB",
            margin: 0,
            width: "100%",
            height: "100vh"
          }
        }}
      />
      <Header />
      <Router>
        <NewSurvey path="/create-survey" />
        <Surveys path="/" />
        <SurveyDetails path="surveys/:surveyId" />
      </Router>
    </>
  );
}

export default App;
