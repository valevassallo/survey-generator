/** @jsx jsx */
import React from "react";
import { Link } from "@reach/router";
import { jsx } from "@emotion/core";

function Header() {
  const headerCss = {
    width: "100%",
    height: 64,
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    background: "#041422"
  };

  return (
    <header css={headerCss}>
      <Link
        css={{ textDecoration: "none", color: "white", fontSize: 25 }}
        to="/"
      >
        Surveys list
      </Link>
      <Link
        css={{ textDecoration: "none", color: "white", fontSize: 25 }}
        to="create-survey"
      >
        Create new survey!
      </Link>
    </header>
  );
}

export default Header;
