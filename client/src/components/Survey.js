/** @jsx jsx */
import React from "react";
import { jsx } from "@emotion/core";
import { Link } from "@reach/router";

function Survey({ survey }) {
  const container = {
    padding: 14,
    margin: 16,
    background: "#FCFCFC",
    mixBlendMode: "hard-light",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 19
  };

  const date = new Date(survey.created_at).toLocaleDateString(undefined, {
    year: "numeric",
    month: "short",
    day: "numeric"
  });

  return (
    <div css={container}>
      <Link
        css={{
          fontWeight: 500,
          fontSize: 30,
          textDecoration: "none",
          color: "black"
        }}
        to={`/surveys/${survey.id}`}
      >
        {survey.title}
      </Link>
      <p css={{ margin: 0, marginTop: 10 }}>
        Number of questions: {survey.questions_number}
      </p>
      <p css={{ margin: 0, marginTop: 10 }}>Creation date: {date}</p>
    </div>
  );
}

export default Survey;
