from rest_framework import serializers
from surveys.models import Survey, Question


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

        read_only_fields = ('id', 'survey',)


class SurveySerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True)

    class Meta:
        model = Survey
        fields = ['id', 'title', 'questions_number', 'created_at', 'questions']

    def create(self, validated_data):
        questions_data = validated_data.pop('questions')
        survey = Survey.objects.create(**validated_data)
        for question_data in questions_data:
            Question.objects.create(survey=survey, **question_data)
        survey.save()
        return survey
