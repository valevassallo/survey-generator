from rest_framework import routers
from surveys.api import SurveyViewSet, QuestionViewSet

router = routers.DefaultRouter()
router.register('api/surveys', SurveyViewSet, 'surveys')
router.register('api/questions', QuestionViewSet, 'questions')

urlpatterns = router.urls
