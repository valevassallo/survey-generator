from django.db import models
from django.utils import timezone


class Survey(models.Model):
    title = models.CharField(max_length=60)
    questions_number = models.IntegerField(default=3)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Question(models.Model):
    QUESTION_TYPES = (
        ('Y/N', 'Y/N'),
        ('SHORT', 'Short Answer'),
        ('LONG', 'Long Answer'),
    )
    survey = models.ForeignKey(
        Survey, on_delete=models.CASCADE, related_name='questions')
    question_type = models.CharField(
        max_length=6, choices=QUESTION_TYPES, default='SHORT')
    actual_question = models.CharField(max_length=200, default='QUESTION')

    def __str__(self):
        return self.actual_question
