# Generated by Django 2.2.5 on 2019-09-15 01:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('surveys', '0002_question'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='actual_question',
            field=models.CharField(default='QUESTION', max_length=200),
        ),
        migrations.AddField(
            model_name='question',
            name='question_type',
            field=models.CharField(choices=[('Y/N', 'Y/N'), ('SHORT', 'Short Answer'), ('LONG', 'Long Answer')], default='SHORT', max_length=6),
        ),
        migrations.AddField(
            model_name='survey',
            name='questions_number',
            field=models.IntegerField(default=3),
        ),
    ]
